import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;

public class HiveCreateDb {
   private static String driverName = "org.apache.hadoop.hive.jdbc.HiveDriver";
private static Statement stmt;
private static Connection con;
   
   public static void main(String[] args) throws SQLException, ClassNotFoundException {
      // Register driver and create driver instance
      try{
      Class.forName(driverName);
      // get connection
      
       con = DriverManager.getConnection("jdbc:hive2://localhost:10000/default", "pankaj", "");
      stmt = con.createStatement();
      
      stmt.executeQuery("CREATE DATABASE IF NOT EXISTS userd");}
      catch(SQLException s){
          
      }
      System.out.println("Database userdb created successfully.");
      String sql = "show databases";
      System.out.println("Running: " + sql);
      ResultSet res = stmt.executeQuery(sql);
      while(res.next()) {
        System.out.println(res.getString(1));
      }
      con.close();
      
     
      }
   }
